import 'package:flutter/material.dart';
import 'package:udemy_course/page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Udemy course 9',
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 1'),
      ),
      body: Container(
        color: Colors.white30,
      ),
      drawer: Drawer(
        child: Container(
          color: Colors.blue[400],
          child: Column(
            children: [
              Container(
                color: Colors.blue[200],
                width: double.infinity,
                height: 200,
                child: Center(
                  child: Text(
                    'Routes',
                    style: TextStyle(
                      fontSize: 40,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white.withOpacity(0.2),
                      spreadRadius: 1,
                      blurRadius: 20,
                      offset: Offset(0, 1),
                    )
                  ],
                ),


                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PageTwo(),
                      ),
                    );
                  },
                  child: Text('Page 2'),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white.withOpacity(0.2),
                      spreadRadius: 1,
                      blurRadius: 20,
                      offset: Offset(0, 1),
                    )
                  ],
                ),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PageThree(),
                      ),
                    );
                  },
                  child: Text('Page 3'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
