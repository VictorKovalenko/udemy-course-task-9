import 'package:flutter/material.dart';
import 'package:udemy_course/page.dart';

class Layout extends StatelessWidget {
  String text;
  Widget? body;

  Layout(this.text, {this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(text),
      ),
      body: body,
      drawer: Drawer(
        child: Container(
          color: Colors.blue[400],
          child: Column(
            children: [
              Container(
                color: Colors.blue[200],
                width: double.infinity,
                height: 200,
                child: Center(
                  child: Text(
                    'Routes',
                    style: TextStyle(
                      fontSize: 40,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageTwo(),
                    ),
                  );
                },
                child: Text('Page 2'),
              ),
              SizedBox(
                height: 50,
              ),
              ElevatedButton(
                onPressed: () {},
                child: Text('Page 3'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
